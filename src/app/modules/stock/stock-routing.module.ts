import { RouterModule, Routes } from '@angular/router';
import { NgModule } from "@angular/core";
import { StockComponent } from './components/stock/stock.component';
import { CreateComponent } from './components/create/create.component';
import { DetailComponent } from './components/detail/detail.component';

const routes: Routes = [
    { path: '', component: StockComponent, children: [
        { path: ':numero', component: DetailComponent } //route avec parametre
    ]}
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class StockRoutingModule {}