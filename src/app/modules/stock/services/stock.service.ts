import { Injectable, OnInit } from '@angular/core';
import { Produit } from 'src/app/models/produit';

@Injectable({
  providedIn: 'root'
})
export class StockService implements OnInit {
  private _stock: Map<string, Produit> = new Map<string, Produit>()

  constructor() { }

  ngOnInit() {
    this._stock.set("2020-0001", new Produit("2020-0001", "Boulangerie", "Pain gris", 42));
  }

  get stock(): Map<string, Produit> {
    return this._stock;
  }

  create(produit: Produit) {
    this._stock.set(produit.numero, produit);
  }

  remove(produit: Produit) {
    this._stock.delete(produit.numero);
  }

  getProduit(numero: string): Produit {
    return this._stock.get(numero);
  }
}
