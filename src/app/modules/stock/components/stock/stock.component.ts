import { Produit } from 'src/app/models/produit';
import { Component, OnInit } from '@angular/core';
import { StockService } from '../../services/stock.service';

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.scss']
})
// StockComponent ~ StockPage => url/stock
export class StockComponent implements OnInit {
  constructor(private stockService: StockService) { }

  ngOnInit(): void {
    this.stock.set("2020-0001", new Produit("2020-0001", "Boulangerie", "Pain gris", 42));
  }

  get stock(): Map<string, Produit> {
    return this.stockService.stock;
  }

  addProduitAction(produit: Produit) {
    if (this.stock.has(produit.numero)) return;    

    this.stock.set(produit.numero, produit);
  }

  removeProduitAction(produit: Produit) {
    if (!this.stock.has(produit.numero)) return;

    this.stock.delete(produit.numero);
  }

}
