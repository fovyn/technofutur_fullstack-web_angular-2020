import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Produit } from 'src/app/models/produit';
import { PRODUIT_FORM_CREATE } from 'src/app/forms/produit.form';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  // Communique vers le parent un produit créé
  @Output() createEvent: EventEmitter<Produit> = new EventEmitter<Produit>();
  formGroup: FormGroup;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group(PRODUIT_FORM_CREATE);
  }

  submitAction() {
    if (this.formGroup.valid) {
      this.createEvent.emit(this.formGroup.value as Produit);
    }
  }

}
