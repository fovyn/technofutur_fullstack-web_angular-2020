import { Produit } from 'src/app/models/produit';
import { StockService } from './../../services/stock.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  produit: Produit;

  constructor(private activatedRoute: ActivatedRoute, private stockService: StockService) { }

  ngOnInit(): void {
    console.log(this.activatedRoute.snapshot.params.numero);

    this.produit = this.stockService.getProduit(this.activatedRoute.snapshot.params.numero);
  }

}
