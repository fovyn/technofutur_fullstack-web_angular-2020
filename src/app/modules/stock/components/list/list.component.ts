import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Produit } from 'src/app/models/produit';
import { StockService } from '../../services/stock.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent {

  constructor(private stockService: StockService) { }

  get items(): Array<Produit> {
    return [...this.stockService.stock.values()];
  }

}
