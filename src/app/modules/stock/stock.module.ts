import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StockComponent } from './components/stock/stock.component';
import { ListComponent } from './components/list/list.component';
import { CreateComponent } from './components/create/create.component';
import { StockRoutingModule } from './stock-routing.module';
import { DetailComponent } from './components/detail/detail.component';
import { SharedModule } from '../shared/shared.module';



@NgModule({
  declarations: [StockComponent, ListComponent, CreateComponent, DetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    StockRoutingModule,
    SharedModule
  ],
  exports: [StockComponent]
})
export class StockModule { }
