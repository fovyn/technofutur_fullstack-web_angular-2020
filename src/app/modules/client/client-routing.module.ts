import { RouterModule, Routes } from '@angular/router';
import { NgModule } from "@angular/core";
import { ClientComponent } from './components/client/client.component';
import { CreateComponent } from './components/create/create.component';

const ROUTES: Routes = [
    { path: '', component: ClientComponent },
    { path: 'create', component: CreateComponent },
    { path: ':controller/:action', component: ClientComponent}
];

@NgModule({
    imports: [RouterModule.forChild(ROUTES)],
    exports: [RouterModule]
})
export class ClientRoutingModule {}