import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientComponent } from './components/client/client.component';
import { CreateComponent } from './components/create/create.component';
import { ClientRoutingModule } from './client-routing.module';



@NgModule({
  declarations: [ClientComponent, CreateComponent],
  imports: [
    CommonModule,
    ClientRoutingModule
  ]
})
export class ClientModule { }
