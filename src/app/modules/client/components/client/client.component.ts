import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {

  constructor(private activitedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    console.log(this.activitedRoute.snapshot);
  }

}
