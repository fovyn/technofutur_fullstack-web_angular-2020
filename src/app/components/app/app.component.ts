import { getQueryPredicate } from '@angular/compiler/src/render3/view/util';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PRODUIT_FORM_CREATE } from 'src/app/forms/produit.form';
import { Produit } from 'src/app/models/produit';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'TFFSWeb2020';

  constructor() {}

  ngOnInit() {
    
  }
}
