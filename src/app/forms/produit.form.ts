import { AbstractControl, FormControl, ValidatorFn, Validators } from '@angular/forms';

export function priceValidator(min: number, max?: number): ValidatorFn {
    return (control: FormControl) => {
        const currentValue = control.value;

        let isValid: boolean = false;
        if (max) {
            isValid = currentValue >= min && currentValue <= max;
        }

        return isValid ? null : {price: `Value must be in [${min},${max ?? Infinity}]`}
    }
}

export const PRODUIT_FORM_CREATE: {[key: string]: AbstractControl} = {
    numero: new FormControl('2020-', [Validators.required]),
    categorie: new FormControl(null, []),
    label: new FormControl('', [Validators.required, Validators.minLength(5)]),
    prix: new FormControl('', [Validators.required, Validators.min(0), priceValidator(0,200)])
}