export class Produit {
    numero: string;
    categorie: string;
    label: string;
    prix: number;

    constructor(numero: string, categorie: string, label: string, prix: number) {
        this.numero = numero;
        this.categorie = categorie;
        this.label = label;
        this.prix = prix;
    }
}